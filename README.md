# eslint-plugin-inclusive-terminology

A plugin for eslint to suggest more inclusive alternative terminology.

# What is this for?
This is a practical approach to use more inclusive terminology in your code. The goal of this project is not to tell you what terminology is right or wrong but instead to help you adjust to changes occurring in our field. Regardless of how you feel about the rules provided by this plugin you may find yourself in a situation where these rules are helpful. This plugin provides each term in its own rule so that you can set the error levels based on your project's/team's needs.

# Installation

Install ESLint locally or globally.

```
$ npm install eslint --save-dev
```

If you installed ESLint globally, install this plugin globally. Otherwise install this plugin locally.
```
$ npm install eslint-plugin-inclusive-terminology
```

# Usage
Use our preset to set all rules to errors.
```
{
  "extends": [
    "eslint:recommended",
    "plugin:inclusive-terminology/all"
  ],
  "plugins": [
    "inclusive-terminology"
  ]
}
```

If you do not use the preset, you can specify individual rules to their own error levels.

```
{
  "plugins": [
    "inclusive-terminology"
  ],
  "rules": {
    "inclusive-terminology/avoid-slave": 2
    "inclusive-terminology/avoid-master": 1
    "inclusive-terminology/avoid-whitelist": 1
    "inclusive-terminology/avoid-blacklist": 2
  }
}
```

# Configuration
You can also replace the internal list of avoided terminology by creating a .inclusive-terminology-rules.json file in the root directory of your project. You can reference the included rules.json file for creating your own rules list.
